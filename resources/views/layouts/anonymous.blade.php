<!doctype html>
<html lang="en">
    <head>
        <title>{{ isset($tags) ? (isset($tags['title']) ? $tags['title'] : '-') : '-' }}</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="{{ isset($tags) ? (isset($tags['description']) ? $tags['description'] : '-') : '-' }}"/>
        <meta name="keywords" content="{{ isset($tags) ? (isset($tags['keyword']) ? $tags['keyword'] : '-') : '-' }}">
        <link rel="icon" href="{{ URL::asset('images/favicon.png') }}" type="image/x-icon">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css"/>

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" crossorigin="anonymous">
        @php
        $cssFiles = array(
                    URL::asset('css/common/bootstrap-font.css'),
                    URL::asset('css/common/bootstrap-material-design.min.css'),
                    URL::asset('css/common/style.css'),
                );
        @endphp

        @if (isset($assets))
            @if(isset($assets['css']) && (count($assets['css']) > 0))
                @foreach($assets['css'] as $css)
                @php array_push($cssFiles, Url('/css').'/'.$css) @endphp
                @endforeach
            @endif
        @endif       
        {!! Minify::stylesheet($cssFiles) !!}

        <!-- JS -->
        <script type="text/javascript">var baseUrl = "{{ url('/') }}";</script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" crossorigin="anonymous"></script>
        @php
        $jsFiles = array(
                    URL::asset('js/common/jquery.validate.min.js'),
                    URL::asset('js/common/global.js'),
                );
        @endphp

        @if (isset($assets))
            @if(isset($assets['js']) && (count($assets['js']) > 0))
                @foreach($assets['js'] as $js)
                @php array_push($jsFiles, Url('/js').'/'.$js) @endphp
                @endforeach
            @endif
        @endif       
        {!! Minify::javascript($jsFiles) !!}

    </head>
    <body>
        <section>   
            @yield('content')
        </section> 
    </body>
</html>
