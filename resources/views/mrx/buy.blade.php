@extends('layouts.vm')
@section('content')
    <h1>MRX Buy Page</h1>
    @if(count($posts) > 0)
        <ul class="list-group">
        @foreach ($posts as $post)
            <li id="product{{$post['post_id']}}" class="list-group-item">
                <div class="image col-md-2 text-center nopadding">
                    <img class="preview-image " src="{{ $post['medispan_details']['image_url'] }}">
                </div>
                <div class="desc col-md-5">
                    <h4 class="col-md-8 ellipsis nopadding nomargin " title="{{ $post['post_title'] }}">
                        {{ $post['post_title'] }}
                    </h4>
                    <h5 class="col-md-4 text-right text-bold ellipsis nomargin nopadding ">
                        {{ $post['ndc_number'] }}                        
                        <span title="{{ $post['post_description'] }}">{{ $post['post_description'] }}</span>
                    </h5>            
                    <div class="condtition">
                        <span class="nomargin">Package Condition</span>
                        <span class="color-primary ng-star-inserted">
                            Original Package - {{ $post['original_package'] }}
                        </span>
                    </div>
                    <div class="col-md-12 nopadding">
                        <p class="col-md-3 nopadding strength">Strength                              
                            <span>                                   
                                <b class="pointer ellipsis inline-block ng-star-inserted" data-html="true" data-toggle="tooltip" title="" data-original-title="{{ $post['package_strength'] }}">{{ $post['package_strength'] }}</b>                    
                            </span>       
                        </p>
                        <p class="col-md-3 ">Exp. Date
                            
                            <b> {{ $post['expire_date'] }}</b>
                        </p>
                        <p class="col-md-6 nopadding ">Packaging
                            
                            <b> {{ $post['medispan_details']['packaging'] }} </b>
                        </p>
                    </div>
                </div>
                <div class="price col-md-3">
                    <div class="col-md-12 nopadding">
                        <p class="col-md-4 nopadding">WAC %
                            
                            <b>{{ $post['this_discount'] }}%</b>
                        </p>
                        <p class="col-md-4 nopadding">Per Unit
                            
                            <b>${{ $post['unit_price'] }}</b>
                        </p>
                        <p class="col-md-4 nopadding ">Price
                            
                            <b class="font-16 ">${{ $post['revised_price'] }}</b>
                        </p>
                    </div>
                    <div class="col-md-12 nopadding">
                        <div class="col-md-6 nopadding ">
                            <p class="pull-left nomargin ">
                                <span class="pull-left">Est. Delivery</span>
                                <span class="login-tooltip pull-left nomargin" data-html="true" data-placement="top" data-toggle="tooltip" title="" data-original-title="Other conditions could impact the delivery date.">
                                    <i class="ico-tooltip"></i>
                                </span>
                                
                                <b>{{ $post['estimate_delivery'] }}</b>
                            </p>
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
        </ul>
    @else
        <div class="alert alert-danger" role="alert">
            No post found!
        </div>        
    @endif
@endsection