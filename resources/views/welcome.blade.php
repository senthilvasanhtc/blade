@extends('layouts.anonymous')
@section('content')

<div class="container">
    <div class="top-right links">
        @auth
            <a href="{{ url('/home') }}">Home</a>
        @else
            <a href="{{ url('mrx-login') }}">Login</a>
            <a href="{{ url('mrx-register') }}">Register</a>
        @endauth
    </div>

<div class="content">
    <div class="title m-b-md">
    {{ __('messages.MRX.WELCOME') }}
    </div>

    <div class="links">
        <a href="https://laravel.com/docs">Documentation</a>
        <a href="https://laracasts.com">Laracasts</a>
        <a href="https://laravel-news.com">News</a>
        <a href="https://forge.laravel.com">Forge</a>
        <a href="https://github.com/laravel/laravel">GitHub</a>
    </div>
</div>
</div>
@endsection
    