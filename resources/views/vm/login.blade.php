@extends('layouts.anonymous')
@section('content')
<div class="">
<div class="container">
<div class="login-box row">
    <div class="col-md-6">
        <div class="login-box-logo">
            <a href="/">
                <img src="images/logo.png">
            </a>
        </div>
    </div>
    <div class="col-md-6 login-box-content pt-2">
        <h4>{{ __('messages.VM.WELCOME') }}</h4>
        <form method="POST" id="vmform" >
            <input type="hidden" name="_token" value="{{ csrf_token('authenticate') }}">
            <div class="form-group">
                <label for="Username" class="bmd-label-floating username-label-floating">Username *</label>
                <input name="username" id="username" autocomplete="off" value="{{ Request::old('username') }}" class="form-control" type="text" max-length="9" required>
                <div class="pull-right login-tooltip" data-html="true" data-placement="top" data-toggle="tooltip" hide="3000" title="" data-original-title="If you do not have a username, enter first initial and last name. <br><br>  (eg. John Smith's username is jsmith )">
                    <i class="ico ico-tooltip"></i>
                </div>
            </div>
            <div class="form-group">
                <label for="Password" class="bmd-label-floating password-label-floating">Password *</label>
                <input type="password" name="password" id="password" value="{{ Request::old('password') }}" class="form-control" required>
                    <span toggle="#password" class="glyphicon glyphicon-eye-open toggle-password"></span>
            </div>
            <div style="margin:12px 0 0 0;">
                <p class="alert alert-danger" id="errorblock">
                    @if(count($errors) > 0)
                        @foreach($errors->getMessages() as $error)
                            {{ $error[0] }}
                        @endforeach
                    @endif
                </p>
            </div>
            <div class="row nomargin">
                <div class="col-xs-1">
                    <button type="submit" name="loginFrom" id="vmbutton" value="vm" class="btn btn-primary btn-raised btn-vm mt-4">Login</button>                      
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <a class="pull-right forgot-link"><i>Forgot Password?</i></a>
                </div>
            </div>
            
        </form>
    </div>
</div>
</div>
</div>
@endsection