<div class="top-right links">
    @if(Session::has('auth_user'))
        <p>{{ Session::get('auth_user')[0]['pharmacy']['dea'] }}</p>
        <p>{{ Session::get('auth_user')[0]['pharmacy']['legal_business_name']}}</p>
        <p>Hi, {{ Session::get('auth_user')[0]['first_name']}}</p>
        <a href="{{ url('/buy') }}">Buy</a>
        <a href="{{ url('/logout') }}">Logout</a>
    @else
        <a href="{{ url('/login') }}">Login</a>
        <a href="{{ url('/register') }}">Register</a>
    @endif
</div>
