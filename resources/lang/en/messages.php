<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Messages
    |--------------------------------------------------------------------------
    |
    */
    
    'MRX' => [
        'WELCOME' => 'Welcome to MatchRX',
    ],
    'VM' => [
        'WELCOME' => 'Welcome to VM',
    ],
    'PHONE' => '(248) 971-0909',
    'TOLL_FREE' => '(877) 590-0808',
    'FAX' => '(248) 971-0910',
    'EMAIL' => 'customerservice@matchrx.com',
    'TIMMING' => '09:00 AM - 06:00 PM EST'
];
