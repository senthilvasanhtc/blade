<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index');
Route::get('deavalidate', 'Controller@deavalidate');

/** MRX Login Module */
Route::get('mrx-login', 'MrxController@login');
Route::post('mrx-login', 'MrxController@postLogin');
Route::post('mrx-forgotpassword', 'MrxController@postForgotpassword');
Route::get('mrx-guestlogin', 'MrxController@guestlogin');
Route::post('mrx-guestlogin', 'MrxController@postGuestlogin');

/** VM Login Module */
Route::get('vm-login', 'VmController@login');
Route::post('vm-login', 'VmController@postLogin');
Route::post('vm-forgotpassword', 'VmController@postForgotpassword');


Route::get('register', function () {
    return view('register');
});
Route::get('/logout', function () {
    Session::flush();
    return redirect('/');
});

/** Authcheck middleware group (Authenticated Routes) */
Route::group(['middleware' => ['checkauth']], function () {

    /** MRX routes */
    Route::prefix('mrx')->group(function(){
        Route::get('buy', 'MrxController@buy');
    });

    /** VM routes */
    Route::prefix('vm')->group(function(){
        Route::get('buy', 'VmController@buy');
    });

});
