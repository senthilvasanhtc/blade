<?php
return [
      'PROJECT' => 'Blade',
      'ALERT_TYPES' => [
        'SUCCESS' => 'S',
        'ERROR' => 'E',
        'INFO' => 'I',
        'WARNING' => 'W',
        'CONFIRM' => 'C',
        'NOTE' => 'N',
        'ALERT' => 'A',
        'CANCEL' => 'CANCEL'
      ],
      'UPDATEINTERVAL' => 180000, // Interval(ms) in which action orders count will be updated.
      'ORDER_ITEM_STATUS' => [
        'AVAILABLE' => 'Available',
        'REVISED' => 'Revised',
        'OUTOFSTOCK' => 'Outofstock',
        'PENDING' => 'Pending',
        'ACCEPTED' => 'Accepted',
        'REJECTED' => 'Rejected',
        'YES' => 'Yes',
        'NO' => 'No'
      ],
      'ORDER_ITEM_STATUS_VIEW' => [
        'Available' => 'Available',
        'Revised' => 'Revised',
        'Outofstock' => 'Out of Stock',
        'Pending' => 'Pending'
      ],
      'ORDER_STATUS' => [
        'CANCELED' => 'Canceled',
        'SCHEDULE_PICKUP' => 'Fedex Schedule',
        'SELLER_REVISED' => 'Seller Revised',
        'BUYER_REVISED' => 'Buyer Revised',
        'PENDING_SELLER_CONFIRMATION' => 'Pending Seller Confirmation',
        'PENDING_FEDEX_DROPOFF' => 'Pending FedEx Drop-Off',
        'PENDING_FEDEX_PICKUP' => 'Pending FedEx Pickup',
        'PENDING_FEDEX_SCHEDULE' => 'Pending FedEx Schedule',
        'INTRANSIT' => 'In-Transit',
        'PENDING_BUYER_CONFIRMATION' => 'Delivered-Pending Buyer Confirmation',
        'ACCEPTED' => 'Accepted',
        'REJECTED' => 'Rejected',
        'COMPLETED' => 'Completed',
        'RETURN' => 'Return',
        'EXCHANGE' => 'Exchange',
      ],
      'SHIPPING_METHODS' => [
        'GROUND' => 'Ground',
        'TWODAY' => '2-Day',
        'OVERNIGHT' => 'Overnight'
      ],
      'NOTICE_TYPES' => [
        'SELLER_CONFIRMED' => 'SaleConfirmed',
        'SELLER_REVISED' => 'OrderRevised',
        'SELLER_CANCELED' => 'OrderCancelled',
        'BUYER_ACCEPTED' => 'BuyerAccepted',
        'BUYER_REJECTED' => 'BuyerRejected',
        'BUYER_REVISED' => 'BuyerRevised',
        'BUYER_CONFIRMED' => 'BuyerConfirmd'
      ],
      'PACKAGE_CONDITION' => [
        'SEALED' => 'sealed',
        'NON_SEALED' => 'nonsealed'
      ],
      'PACKAGE_CONDITION_VIEW' => [
        'sealed' => 'Sealed',
        'nonsealed' => 'Non Sealed'
      ],
      'DROP_OFF_AT' => 'Drop off at a FedEx office location',
      'FEDEX_TERMS_URL' => 'http://www.fedex.com/cm/shippingguide/terms/#18',
      'FEDEX_PICKUP_COST' => 2.50,
      'OTHER_PACKAGE_CONDITION' => [
        1 => 'Torn Package Label',
        2 =>  '“X” on Container',
        3 => 'Sticker / Glue Residue',
        4 => 'Writing on Container'
      ]
];
