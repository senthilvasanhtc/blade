<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Helper;

class VmController extends Controller
{
    /**
     * MRX Login 
     */    
    public function login()
    {
        $auth_user = Session::get('auth_user');       
        if (!empty($auth_user)) {
            if(Helper::authRedirect())
                return redirect(Helper::authRedirect());
        }   

        $tags = [
            'title' => 'VM - Login',
            'description' => 'VM :: Login to your account.',
            'keyword' => 'matchrx, VM, login'
        ];

        $assets = [
            'js' => ['common/formValidations.js', 'vm/login.js']
        ];

        return view('vm/login')->with(['tags' => $tags, 'assets' => $assets]);
    }

    /**
     * MRX Post Login
     */
    public function postlogin(Request $request)
    {
        $auth_user = Session::get('auth_user');       
        if (!empty($auth_user)) {
            if(Helper::authRedirect())
                return redirect(Helper::authRedirect());
        }
        //URL TO BE CALLED
        $url = Helper::getApiBase('VM').'/users/login';
        //PARAMS to be passed to the URL
        $params = $request->only(['username', 'password', 'loginFrom']);
        $params['showErrorMessage'] = false;
        //CALL the API
        $response = Helper::curl('POST', $url, $params);                

        //PROCESS response and redirect
        if (isset($response['data']['message'])) {  
            return redirect('login')->withInput()->withErrors($response['data']['message']);
        } elseif (isset($response['data']['error'])) {  
            return redirect('login')->withInput()->withErrors($response['data']['error']);
        } elseif (isset($response['message'])) {  
            return redirect('login')->withInput()->withErrors($response['message']);
        } else {
            //SUCESS LOGIN
            $response['data']['login_type'] = 'vm';
            Session::push('auth_user', $response['data']);           
            return redirect('vm/buy');
        }
    }

    public function buy(Request $request)
    {
        $auth_user = Session::get('auth_user');       
        $url = Helper::getApiBase().'/posts/showmarketplace';
        $params = [];
        $headers = Helper::getAuthHeader(Helper::getAccessToken());
        $response = Helper::curl('POST', $url, $params, $headers, true);         
        $response = json_decode($response, true);  
        $post = $response['data']['marketplace']['data'];
        return view('vm/buy', ['posts' => $post]);
    }
    
}
