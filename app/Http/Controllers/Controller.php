<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Session;
use Helper;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Home Screen
     */
    public function index()
    {
        $auth_user = Session::get('auth_user');   
        if (!empty($auth_user)) {
            if(Helper::authRedirect())
                return redirect(Helper::authRedirect());
        }

        $tags = [
            'title' => 'Matchrx pharmacy B2B',
            'description' => 'Lorem Ipsum? Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
            'keyword' => 'matchrx, vm'
        ];

        $assets = [
            'css' => ['common/sample.css'],
            'js' => ['common/sample.js']
        ];

        return view('welcome')->with(['tags' => $tags, 'assets' => $assets]);
    }

    
    public function deavalidate(Request $request)
    {   
        //PARAMS to be passed to the URL
        $params = $request->input('dea');
        $url = Helper::getApiBase().'/users/deavalidate?'.$params;
        //CALL the API
        $response = Helper::curl('GET', $url, '');   
        return response()->json($response);

    }

}
