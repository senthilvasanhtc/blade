<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth_user = Session::get('auth_user');       
        if (empty($auth_user) || !isset($auth_user[0]['access_token'])) {
            return redirect('login');
        }

        return $next($request);
    }
}
