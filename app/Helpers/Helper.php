<?php
namespace App\Helpers;
use Session;

class Helper {

    /**
     * CURL
     */
    public static function curl($method, $url, $params = [], $headers = [], $json = false) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        if ($json)
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        else 
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $output = curl_exec($ch);
        curl_close($ch);
        if ($json) {
            return $output;
        } else {
            return json_decode($output, 'true');
        }
    }

    /**
     * Get Access Token
     */
    public static function getAccessToken() {
        $auth_user = Session::get('auth_user');       
        if (isset($auth_user) && isset($auth_user[0]['access_token']) && ($auth_user[0]['access_token'] != '')) {
            return $auth_user[0]['access_token'];
        }
        return;
    }

    /**
     * Get loggedin user Type (Mrx / VM Buyer /  VM Seller)
     */
    public static function getLoginType() {
        $auth_user = Session::get('auth_user');       
        if (isset($auth_user) && isset($auth_user[0]['login_type']) && ($auth_user[0]['login_type'] != '')) {
            return $auth_user[0]['login_type'];
        }
        return;
    }

    /**
     * Redirect to appropriate URL if user session is maintained
     */
    public static function authRedirect() {
        if(Self::getLoginType() && (Self::getLoginType() == 'mrx'))
            return 'mrx/buy';
        else if(Self::getLoginType() && (Self::getLoginType() == 'vm'))
            return 'vm/buy';
        else
            return;
    }

    /**
     * Set Authorization Header
     */
    public static function getAuthHeader($access_token) {
        $headers = [
            'Content-Type:application/json',
            'Authorization:Bearer' . ' ' . $access_token
        ];

        return $headers;
    }

    /**
     * Get Base API url
     */
    public static function getApiBase($mode='MRX'){
        if($mode === 'MRX')
            return env('API_MRX_BASEURI');
        else
            return env('API_VM_BASEURI');
    }


}
