$(document).ready(function(){

    $('body').bootstrapMaterialDesign(); 
    $('[data-toggle="tooltip"]').tooltip();

// End of documentReady
});    

/**
 * Ajax request handler
 * @param {*} method 
 * @param {*} url 
 * @param {*} params 
 * @param {*} dataType 
 */
function ajaxRequest(method, url, params='', dataType='json') {
    $.ajax({
        type: method,
        url: url,
        data: params,
        dataType: dataType,
        success : function(result) {  
            if(result)                       
                return result;
            else
                return;
        },
        error : function(result) {                         
            if(result)
                return result;
            else
                return;
        }
    });
}

/**
 * DEA validation
 * @param {object} obj 
 */
function validateDEA(obj){
    if ($.trim(obj.value) != '') {
        var result = ajaxRequest('GET', baseUrl+'/login/deavalidate', {'dea':obj.value});
        if(result) {
            if (result.status == 'OK') {
                return result.data.store_flag;
            }
        }
        return;
    }
}

