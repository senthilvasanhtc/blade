$.validator.addMethod("dea_pattern", function(value, element) {
    var re = new RegExp("^[A-z][a-zA-Z0-9]([0-9]{7})$");
    return this.optional(element) || re.test(value);
});

function highlightError(element, errorClass, validClass) {
        $('body').find("."+element.id + "-label-floating").addClass("errormsg");
        $('body').find("#"+element.id).addClass('error');
}

function unhighlightError(element, errorClass, validClass) {
        $('body').find("."+element.id + "-label-floating").removeClass("errormsg");
        $('body').find("#"+element.id).removeClass('error');
}