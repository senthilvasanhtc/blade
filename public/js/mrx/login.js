$(document).ready(function(){

/* ---- Login page ---- */
    $("#mrxform").validate({
        highlight: function(element, errorClass, validClass){ highlightError(element, errorClass, validClass); },
        unhighlight: function(element, errorClass, validClass){ unhighlightError(element, errorClass, validClass) },
        focusInvalid: false,
        rules: {
        dea: {
            required: true,
            dea_pattern: true
        },
        username: {
            required: true,
            minlength: 6
        },
        password: {
            required: true
        },
        },
        messages:{
            dea: {
                required: "DEA # is required",
                dea_pattern: 'DEA # is invalid, example (AB1234567/A01234567).'
            },
            username: {
                required: "Username is required",
                minlength: "Username should be minimum 5 characters"
            },
            password: {
                required: "Password is required"
            }
        }
    });

    $("#dea").focus(function () { 
        if(validateDEA(this)) {              
            if (validateDEA(this) == 'both') {
                $('#mrxbutton').removeAttr('disabled');
                $('#mrxbutton').removeClass('disabled');
                $('#vmbutton').removeAttr('disabled');
                $('#vmbutton').removeClass('disabled');
            } else if (validateDEA(this) == 'mrx') {
                $('#mrxbutton').removeAttr('disabled');
                $('#mrxbutton').removeClass('disabled');
                $('#vmbutton').attr("disabled", "disabled");
                $('#vmbutton').addClass('disabled');
            } else if (validateDEA(this) == 'vmbuyer') {
                $('#mrxbutton').attr("disabled", "disabled");
                $('#mrxbutton').addClass('disabled');
                $('#vmbutton').removeAttr('disabled');
                $('#vmbutton').removeClass('disabled');                                    
            }
        }
    }); 

    $("#dea").blur(function () {     
        if(validateDEA(this)) {           
            if (validateDEA(this) == 'both') {
                $('#mrxbutton').removeAttr('disabled');
                $('#mrxbutton').removeClass('disabled');
                $('#vmbutton').removeAttr('disabled');
                $('#vmbutton').removeClass('disabled');
            } else if (validateDEA(this) == 'mrx') {
                $('#mrxbutton').removeAttr('disabled');
                $('#mrxbutton').removeClass('disabled');
                $('#vmbutton').attr("disabled", "disabled");
                $('#vmbutton').addClass('disabled');
            } else if (validateDEA(this) == 'vmbuyer') {
                $('#mrxbutton').attr("disabled", "disabled");
                $('#mrxbutton').addClass('disabled');
                $('#vmbutton').removeAttr('disabled');
                $('#vmbutton').removeClass('disabled');                                    
            }
        }
    }); 

    $(".toggle-password").click(function() {
        $(this).toggleClass("glyphicon-eye-close");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

    if ($('#errorblock').text().trim().length == 0) {
        $('#errorblock').hide();
    }   

    if($('#dea').val() != '') {
        $('#dea').focus();
    }

/* ---- End of Login page */



// End of documentReady
});