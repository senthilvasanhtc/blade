$(document).ready(function(){

/* ---- Login page ---- */

    $("#vmform").validate({
        highlight: function(element, errorClass, validClass){ highlightError(element, errorClass, validClass); },
        unhighlight: function(element, errorClass, validClass){ unhighlightError(element, errorClass, validClass) },
        focusInvalid: false,
        rules: {
        username: {
            required: true,
            minlength: 6
        },
        password: {
            required: true
        },
        },
        messages:{
            username: {
                required: "Username is required",
                minlength: "Username should be minimum 5 characters"
            },
            password: {
                required: "Password is required"
            }
        }
    });

    $(".toggle-password").click(function() {
        $(this).toggleClass("glyphicon-eye-close");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

    if ($('#errorblock').text().trim().length == 0) {
        $('#errorblock').hide();
    }   

    if($('#dea').val() != '') {
        $('#dea').focus();
    }

/* ---- End of Login page */



// End of documentReady
});